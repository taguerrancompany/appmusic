import 'package:app_music/Screens/HomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

import 'Screens/SongScreen.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(
          bodyColor: Colors.white,
          displayColor: Colors.purple
        ),


      ),
      home: const SongScreen(),
      getPages: [
        GetPage(name: "/", page: ()=> const HomeScreen()),
        GetPage(name: "/song", page: ()=> const SongScreen()),
        GetPage(name: "/teste", page: ()=> const HomeScreen())
      ],
    );
  }
}
