import 'Songs.dart';

class PlayList {
  late final String title;
  late final List<Song> songs;
  late final String imageUrl;

  PlayList(
      {
        required this.title,
        required this.songs,
        required this.imageUrl
      });

 static List<PlayList> playList = [
    PlayList(
        title: "rock",
        songs: Song.songs,
        imageUrl:"assets/imgs/solo.jpg" ),
    PlayList(title: "rock",
        songs: Song.songs,
        imageUrl:"assets/imgs/solo.jpg" )
  ];
}