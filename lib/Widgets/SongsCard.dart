import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../Models/Songs.dart';

class SongsCard extends StatelessWidget {
  const SongsCard({
    Key? key,
    required this.songs,
  }) : super(key: key);

  final Song songs;



  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Get.toNamed("/song",arguments: songs);
      },
      child:  Container(
        margin: const EdgeInsets.only(right: 10),
        child: Stack(
            alignment: Alignment.bottomCenter,
            children:[
              Container(
                width: MediaQuery.of(context).size.width  *0.45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    image: DecorationImage(image: AssetImage(songs.coverUrl),
                        fit: BoxFit.cover)
                ),
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width  *0.45,
                margin: const EdgeInsets.only(bottom: 0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    color: Colors.white.withOpacity(0.6)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(songs.title,
                          style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                              color: Colors.purple,
                              fontWeight: FontWeight.bold
                          ),),
                        Text(
                          songs.descripition,
                          style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.bold
                          ),)
                      ],
                    ) ,

                    const Icon(
                      Icons.play_circle,
                      color: Colors.deepPurple,
                    )
                  ],
                ),
              ),]
        ),
      ),
    );
  }
}