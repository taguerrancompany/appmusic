import 'package:app_music/Models/PlayList.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../Models/Songs.dart';
import '../Widgets/PlayLIstCard.dart';
import '../Widgets/SectionHeader.dart';
import '../Widgets/SongsCard.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});



  @override
  Widget build(BuildContext context) {
    List<Song> songs = Song.songs;
    List<PlayList> playList = PlayList.playList;

    return (

  Container(
    decoration:  BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          Colors.deepPurple.shade800.withOpacity(0.8),
          Colors.purple.shade500.withOpacity(0.8)
        ]

      )
    ),
    child:  Scaffold(
      backgroundColor: Colors.transparent,
        appBar: const _AppBarWidget(),
        bottomNavigationBar: const _bootonNavi(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              const _DiscoveryMusic(),
               TopMusic(songs: songs),
              Padding(padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  const SectionHeader(title: "Play list"),
                  ListView.builder(
                    shrinkWrap: true,
                      padding: const EdgeInsets.only(top: 20.0),
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: playList.length,
                      itemBuilder: ((context,index){
                        return PlayListCard(playList: playList[index]);
                      }),
                  )
                ],
              ),)

            ],
          ),
        ),
      ),
    )
    );


  }
}


class TopMusic extends StatelessWidget {
  const TopMusic({
    Key? key,
    required this.songs,
  }) : super(key: key);

  final List<Song> songs;

  @override
  Widget build(BuildContext context) {
    return Padding(padding:  const EdgeInsets.only(
     top: 20.0,
     left: 20.0,
     bottom: 20.0
              ),
              child:  Column(
     children:  [
       const Padding(padding: EdgeInsets.only(right: 20.0),
         child:   SectionHeader(title: "Em alta!"),),
       const SizedBox(height: 20.0,),
       SizedBox(height: MediaQuery.of(context).size.height *0.27,
         child: ListView.builder(
             scrollDirection: Axis.horizontal,
             itemCount: songs.length,
             itemBuilder: (context,index){
               return SongsCard(songs: songs[index]);
             }),)
     ],
              ),
    );
  }
}





class _DiscoveryMusic extends StatelessWidget {
  const _DiscoveryMusic({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Bem vindo!",
            style: Theme.of(context).textTheme.bodyLarge
          ), Text(
            "Adicione suas musicas favoritas",
            style: Theme.of(context)
            .textTheme
            .headline6!
            .copyWith(fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 20),
          TextFormField(
            decoration: InputDecoration(
              isDense: true,
              filled: true,
              fillColor: Colors.white,
              hintText: 'pesquisar',
              hintStyle: Theme.of(context)
                .textTheme.bodyMedium!
                  .copyWith(color: Colors.grey.shade400),
              prefixIcon:  Icon(Icons.search,
                color: Colors.grey.shade400),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
                borderSide: BorderSide.none
              )
            ),
          )
        ],
      ),
    );
  }
}

class _bootonNavi extends StatelessWidget {
  const _bootonNavi({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      backgroundColor: Colors.deepPurple.shade900,
      unselectedItemColor: Colors.white,
      selectedItemColor: Colors.white,
      items: const [
      BottomNavigationBarItem(
          icon: Icon(Icons.home),
      label: "home"),
      BottomNavigationBarItem(
          icon: Icon(Icons.favorite_border_outlined),
          label: "home"),
      BottomNavigationBarItem(
          icon: Icon(Icons.play_circle_filled_outlined),
          label: "home"),
      BottomNavigationBarItem(
          icon: Icon(Icons.people_outline_outlined),
          label: "home"),
    ],);
  }
}

class _AppBarWidget extends StatelessWidget with PreferredSizeWidget {
  const _AppBarWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      leading: const Icon(Icons.grid_view_rounded),
      actions: [
        Container(
          margin: const EdgeInsets.only(right: 20),
          child: const CircleAvatar(
            backgroundImage: NetworkImage("https://www.shutterstock.com/image-illustration/avatar-modern-young-guy-working-260nw-2015853839.jpg"),
          ),
        )
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(56.0);
}
